### author: haolin yan
### time: 2021.5.9
import torch
from torch import linalg as LA
import numpy as np
from transformers import AutoModel,  AutoTokenizer
###first step we aim to make img similar to its caption in vector space
class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.vision_model = torch.hub.load('facebookresearch/swav', 'resnet50')
        self.vision_model.fc = torch.nn.Linear(2048, 768)
        self.language_model = AutoModel.from_pretrained("princeton-nlp/sup-simcse-bert-base-uncased").eval()
        self.tokenizer = tokenizer = AutoTokenizer.from_pretrained("princeton-nlp/sup-simcse-bert-base-uncased")
    def forward(self, data):
        r"""
        data:[(batchsize, 3, 224, 224), 
        """
        img = data[0]
        texts = data[1]
        inputs = self.tokenizer(texts, padding=True, truncation=True, return_tensors="pt")
        for k,v in inputs.items():
            inputs[k] = v.cuda()
        img_vec = self.vision_model(img)
        cap_vec = self.language_model(**inputs, output_hidden_states=True, return_dict=True).pooler_output
        
        return img_vec, cap_vec.detach_()
    
class simi_loss():
    def __init__(self, t = 0.01):
        self.t = t
    def __call__(self, img_vec, cap_vec):
        return -1 * torch.log(torch.exp(torch.mean(self.cosine(img_vec, cap_vec) / self.t)))
    def cosine(self, vec1, vec2):
        r"""
        vec (bat,d)
        return (bat,)
        """
        v1norm = LA.norm(vec1, dim = 1).view(-1)
        v2norm = LA.norm(vec2, dim = 1).view(-1)
        return torch.sum(vec1 * vec2, dim = 1) / (v1norm * v2norm)
    

class metric():
    def __init__(self):
        self.acc = []
        
    def compute(self, vec1, vec2):
        v1norm = LA.norm(vec1, dim = 1).view(-1)
        v2norm = LA.norm(vec2, dim = 1).view(-1)
        return torch.mean(0.5 * torch.sum(vec1 * vec2, dim = 1) / (v1norm * v2norm) + 0.5) 
        
    def __call__(self):
        return np.mean(self.acc)
    
    def update(self, acc):
        self.acc.append(acc)
    @property
    def reset(self):
        self.acc = []
    
#cap = torch.arange(12).view(3, 4).to(torch.int64)
#img = torch.ones(1,3,224,224).to(torch.float32)
#tokenizer = AutoTokenizer.from_pretrained("princeton-nlp/sup-simcse-bert-base-uncased")
#texts = ["hello","dog","cat"]
#inputs = tokenizer(texts, padding=True, truncation=True, return_tensors="pt")

#model = Model()
#loss = simi_loss()
#img_vec, cap_vec = model([img, inputs])
#print(loss(img_vec, cap_vec))